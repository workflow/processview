processview.core.manager package
================================

Subpackages
-----------

.. toctree::

   processview.core.manager.test

Submodules
----------

processview.core.manager.manager module
---------------------------------------

.. automodule:: processview.core.manager.manager
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: processview.core.manager
   :members:
   :undoc-members:
   :show-inheritance:
