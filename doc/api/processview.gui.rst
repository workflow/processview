processview.gui package
=======================

Submodules
----------

processview.gui.icons module
----------------------------

.. automodule:: processview.gui.icons
   :members:
   :undoc-members:
   :show-inheritance:

processview.gui.processmanager module
-------------------------------------

.. automodule:: processview.gui.processmanager
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: processview.gui
   :members:
   :undoc-members:
   :show-inheritance:
