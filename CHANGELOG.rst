Change Log
==========

1.5.0: 11/2024
--------------

* add an option to rename processes

1.4.0: 11/2024
--------------

* fix clear of a dataset
* Add an option to select how to order datasets

1.3.0: 07/2023
--------------

* add option to remove some dataset from the supervisor

1.2.0: 05/2023
--------------

* add dataset sorting

1.0.0: 07/2022
--------------

* update identifier API to fit the tomoscan 'BaseIdentifier' API
* add dataset url copy from right click.

0.3: 10/2021
------------

* add get_process function
* remove ExternalRessources

0.2.2:
------

* core
    * add helpers functions

0.2.1:
------

* gui
    * update column when a process is removed

0.2.0:
------

* core
    * DatasetState: add `WAIT_USER_VALIDATION` state


0.1.0:
------

* core:
    * dataset: add `Dataset` and `DatasetIdentifier` classes. API for datasets
    * superviseprocess: add `SuperviseProcess` class. API for processes
    * manager: add `ProcessManager` and `DatasetState` classes. Allow to define interaction between processes and datasets

* gui
    * processmanager
        * simple gui to display process and datasets states.
        * allow reprocessing of a dataset
